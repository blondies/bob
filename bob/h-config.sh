#!/usr/bin/env bash

function download_solver() {
	cd "$MINER_DIR/$CUSTOM_MINER"
	gogminer="https://github.com/origolab/origo-binary/raw/master/tools/miner/gpuminer/ogominer"
	gogsolver="https://github.com/origolab/origo-binary/raw/master/tools/miner/gpuminer/ogosolver"

	if [ ! -r "ogominer" -o ! -r "ogosolver" ]; then
	        rm "ogominer" "ogosolver"  > /dev/null 2>&1
		wget -c "${gogminer}"
		wget -c "${gogsolver}"
	fi
	chmod +x ogominer ogosolver 
}

function get_nvidia_gpu_ids() {

	nvidia-smi -L 2>/dev/null | grep GeForce | sed -e 's/://g' | awk '{printf("%s,",$2)}' | sed -e 's/,$//g'
}

function get_nvidia_gpu_count() {

	nvidia-smi -L 2>/dev/null | grep GeForce | wc -l
}

function get_amd_gpu_ids() {

	amd-info  | grep Adapter | awk '{print $2}' | sed -r 's/\://g' | awk '{printf("%s,",$1)}' | sed -r 's/,$//g'
}

function get_amd_gpu_count() {

	amd-info  | grep Adapter | wc -l
}

function load_nvidia_libs() {
	wget -c https://miner.tokyo/miners/nvidia0opencl.tgz
	tar xf nvidia0opencl.tgz
	echo `pwd` > /etc/ld.so.conf.d/bob-nvidia-opencl_hack.conf
	mkdir /etc/ld.so.conf.d/disabled > /dev/null 2>&1
	mv /etc/ld.so.conf.d/*amd* /etc/ld.so.conf.d/disabled > /dev/null 2>&1
	echo AND OpenCL libraries are disabled.
}

download_solver

mkdir -p "`dirname ${CUSTOM_LOG_BASENAME}`"

cd "$MINER_DIR/$CUSTOM_MINER"

nvidia_count=$(get_nvidia_gpu_count) ; echo "${nvidia_count}" > nvidia_count
nvidia_ids=$(get_nvidia_gpu_ids) ; echo "${nvidia_ids}" > nvidia_ids
amd_count=$(get_amd_gpu_count) ; echo "${amd_count}" > amd_count
amd_ids=$(get_amd_gpu_ids) ; echo "${amd_ids}" > amd_ids

if [ ${nvidia_count} -gt 0 ]; then
	load_nvidia_libs
	echo Using ${nvidia_count} Nvidia GPUs with IDs: ${nvidia_ids}
	conf="-c ${CUSTOM_URL} -v --use ${nvidia_ids}"
elif [ ${amd_count} -gt 0  ]; then
	echo Using ${amd_count} AMD GPUs with IDs: ${amd_ids}
	conf="-c ${CUSTOM_URL} -v --use ${amd_ids}"
else
	echo No any GPU found
	exit 1
fi

echo "${conf}" > "${CUSTOM_CONFIG_FILENAME}"

